const Telegraf = require('telegraf')
const Telegram = require('telegraf/telegram')
const Extra = require('telegraf/extra')
const Utils = require('./utils')
const Storage = require('./database')
const express = require('express')
const bodyParser = require('body-parser')
const port = 3000

// Инициализация БД
Storage.init().then(console.log).catch(console.log)

// Инциализация Сервера
const srv = express()
srv.use(bodyParser.json())

// Инициализация бота
const bot = new Telegraf('637651867:AAF3Rt0Vy-0F0isI1mr8Tgi9Yyr7ujKu1b0')
const telegram = new Telegram('637651867:AAF3Rt0Vy-0F0isI1mr8Tgi9Yyr7ujKu1b0')

// server handlers
srv.post('/bug-report', (req, res) => {
  Storage.addReport(req.body).then(users => {
    users.forEach(user => telegram.sendMessage(user.chat_id, Utils.reportToText(req.body)))
  }).catch(console.log)
  res.send('It\'s okay!')
})

srv.listen(port, () => console.log(`App listening on port ${port}!`))
// end server handlers


// bot handlers
bot.start((ctx) => ctx.reply('Welcome! Say bot "/help" and he help you :)'))

bot.help(ctx => ctx.reply('Bot has next skills:\n\n/get_all - getting all reports of bugs\n\n ' +
    'And also you will be get new bug reports from users in realtime 😎'))

bot.command('get_all', ctx => {
  Storage.getAllReports().then(reports => {
    reports.forEach(function (row) {
      ctx.reply(Utils.reportToText(row))
    })
  }).catch(console.log)
})

bot.command('subscribe', ctx => {
  Storage.updateSubscriptionUser({
    id: ctx.from.id,
    name: ctx.from.username,
    chat_id: ctx.chat.id,
    is_subscribe: 1
  }).then(() => ctx.reply('Okay! You subscribed 👌'))
      .catch(err => ctx.reply('Sorry, an error occurred ‼️🚑\nDescription: ' + err.message))
})

bot.command('unsubscribe', ctx => {
  Storage.updateSubscriptionUser({
    id: ctx.from.id,
    name: ctx.from.username,
    chat_id: ctx.chat.id,
    is_subscribe: 0
  }).then(() => ctx.reply('Okay! You unsubscribed 👌'))
      .catch(err => ctx.reply('Sorry, an error occurred ‼️🚑\nDescription: ' + err.message))
})
// end bot handlers

// bot.help((ctx) => ctx.reply('Send me a sticker'))
/*bot.on('sticker', (ctx) => ctx.reply('👍'))
bot.hears('hi', (ctx) => ctx.reply('Hey there'))
bot.hears(/buy/i, (ctx) => ctx.reply('Buy-buy'))*/

bot.startPolling()
