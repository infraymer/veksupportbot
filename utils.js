const date = require('date-and-time')

module.exports.stringToDate = function (str) {
  str = str.split('-')
  return new Date(str[2], str[1], str[0])
}

module.exports.dateToString = function (date) {
  return `${date.getDay()}-${date.getMonth()}-${date.getFullYear()}`
}

module.exports.reportToText = function (report) {
  return `${'\u260e'} ${report.phone}\n\n` +
      `${report.text}\n\n` +
      `brand: ${report.brand}\n` +
      `device: ${report.device}\n` +
      `hardware: ${report.hardware}\n` +
      `sdk: ${report.sdk}\n` +
      `${'\u23f0'} ${date.format(new Date(report.time), 'HH:mm DD-MM-YYYY')}`
}