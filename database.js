const sqlite = require('sqlite')
const SQL = require('sql-template-strings')
let db = sqlite.open('./maindb.db', {Promise})

module.exports.init = async () => db = await db

module.exports.getAllReports = async function () {
  return await db.all('SELECT * FROM bug_report ORDER BY time ASC')
}

module.exports.addReport = async function (report) {
  await db.run(SQL`
  INSERT INTO bug_report(brand, device, hardware, phone, sdk, text, time, user_id) 
  VALUES (${report.brand}, ${report.device}, ${report.hardware}, ${report.phone}, ${report.sdk}, ${report.text}, ${report.time}, ${report.user_id})`)
  return await db.all('SELECT * FROM user WHERE is_subscribe = 1')
}

module.exports.updateSubscriptionUser = async function (user) {
  await db.all(SQL`
  INSERT OR REPLACE INTO user(user_id, chat_id, name, is_subscribe) 
  VALUES (${user.id}, ${user.chat_id}, ${user.name}, ${user.is_subscribe})`)
}

module.exports.getSubscribers = async function () {
  return await db.all('SELECT * FROM user WHERE is_subscribe = 1')
}


